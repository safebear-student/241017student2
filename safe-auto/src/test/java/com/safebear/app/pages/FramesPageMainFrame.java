package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 24/10/2017.
 */
public class FramesPageMainFrame {

    WebDriver driver;

    public FramesPageMainFrame(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Sign In");
    }
}
